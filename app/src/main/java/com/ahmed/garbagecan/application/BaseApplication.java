package com.ahmed.garbagecan.application;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Ahmed on 6/6/2017.
 */

public class BaseApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
