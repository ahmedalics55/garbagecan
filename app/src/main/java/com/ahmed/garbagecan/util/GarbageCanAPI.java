package com.ahmed.garbagecan.util;

import com.ahmed.garbagecan.model.entity.Customer;
import com.ahmed.garbagecan.model.entity.Sale;
import com.ahmed.garbagecan.model.entity.SalePictures;
import com.ahmed.garbagecan.model.request.AddCustomerRequest;
import com.ahmed.garbagecan.model.request.AddSaleRequest;
import com.ahmed.garbagecan.model.request.InitRequest;
import com.ahmed.garbagecan.model.response.GenericResponse;
import com.ahmed.garbagecan.model.response.InitResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface GarbageCanAPI {

    @POST("Init")
    Call<GenericResponse<InitResponse>> init(@Body InitRequest request);

    @POST("AddCustomer")
    Call<GenericResponse<Customer>> addCustomer(@Body AddCustomerRequest request);

    @POST("AddSale")
    Call<GenericResponse<Sale>> addSale(@Body AddSaleRequest request);

    @Multipart
    @POST("AddSalePictures")
    Call<GenericResponse<SalePictures>> upload(@Part("saleID") int saleID, @Part MultipartBody.Part file);

    @Multipart
    @POST("AddSalePictures")
    Call<GenericResponse<SalePictures>> upload(@Part MultipartBody.Part file);

}
