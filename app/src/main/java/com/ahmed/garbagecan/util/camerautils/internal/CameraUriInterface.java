package com.ahmed.garbagecan.util.camerautils.internal;

/**
 * @author Aidan Follestad (afollestad)
 */
interface CameraUriInterface {

    String getOutputUri();
}
