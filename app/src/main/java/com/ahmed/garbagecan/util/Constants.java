package com.ahmed.garbagecan.util;

import com.ahmed.garbagecan.model.entity.Customer;
import com.ahmed.garbagecan.model.entity.Info;
import com.ahmed.garbagecan.model.response.InitResponse;

/**
 * Created by Ahmed on 4/29/2017.
 */

public class Constants {
    public static final String API_URL = "http://192.168.10.36:1122/Student/";
    public static String DEVICE_ID;
    public static String TAG = "ahmed_tag_x";

    public static Customer LOGGED_IN_CUSTOMER = null;
    public static Info INFO = null;
    public static boolean IS_APP_INITIALIZED = false;
}
