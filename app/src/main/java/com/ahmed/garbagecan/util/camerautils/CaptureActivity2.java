package com.ahmed.garbagecan.util.camerautils;

import android.app.Fragment;
import android.support.annotation.NonNull;

import com.ahmed.garbagecan.util.camerautils.internal.BaseCaptureActivity;
import com.ahmed.garbagecan.util.camerautils.internal.Camera2Fragment;


public class CaptureActivity2 extends BaseCaptureActivity {



    @Override
    @NonNull
    public Fragment getFragment() {
        return Camera2Fragment.newInstance();
    }
}