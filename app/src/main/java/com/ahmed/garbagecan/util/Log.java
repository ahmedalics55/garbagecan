package com.ahmed.garbagecan.util;

import android.content.IntentSender;

public class Log {
    static final boolean LOG = true;

    public static void i(String string) {
        if (LOG) {
            if(string== null) string = "---null string for log";
            android.util.Log.i(Constants.TAG, string);
        }
    }

}
