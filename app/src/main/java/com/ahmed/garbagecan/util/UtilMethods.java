package com.ahmed.garbagecan.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.widget.EditText;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Ahmed on 4/29/2017.
 */

public class UtilMethods {

    static final int THUMBNAIL_SIZE = 400;

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public synchronized static String id(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }

        return uniqueID;
    }

    public static String uriToBase64(Activity activity, Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE + (THUMBNAIL_SIZE/4), false);
        } catch (IOException e) {
            e.printStackTrace();
        }


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String base64 = android.util.Base64.encodeToString(b, android.util.Base64.DEFAULT);
        return base64;
    }

    public static byte[] bitmapToBytes(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        return b;
    }

    public static Bitmap base64toBitmap(String base64){
        try {
            byte[] decodedString = android.util.Base64.decode(base64, android.util.Base64.DEFAULT);
            return byteArrayToBitmap(decodedString);
        }
        catch (Exception e) { return null; }
    }

    public static Bitmap byteArrayToBitmap(byte[] bytes){
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(270);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public static Bitmap rotateOnceBitmap(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public static Bitmap getSmallBitmap(String base64) {

        try
        {

            Bitmap thumbnail = base64toBitmap(base64);
            return getSmallBitmap(thumbnail);
        }
        catch(Exception ex) {
            return null;
        }

    }

    public static Bitmap getSmallBitmap(Bitmap thumbnail) {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, THUMBNAIL_SIZE, THUMBNAIL_SIZE + (THUMBNAIL_SIZE/4), false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return thumbnail;
    }

    public static void showToast(Context ctx, String msg){
        if(ctx==null) return  ;
        Toast.makeText(ctx, msg,Toast.LENGTH_SHORT).show();
    }

    public static void showFailureToast(Context ctx){
        if(ctx==null) return  ;
        Toast.makeText(ctx, "Unknown error occured. Please, check your Internet connection",Toast.LENGTH_LONG).show();
    }

    public static void showToastLong(Context ctx, String msg){
        if(ctx==null) return  ;
        Toast.makeText(ctx, msg,Toast.LENGTH_LONG).show();
    }

    public static void showEmptyToast(Context ctx){
        if(ctx==null) return  ;
        Toast.makeText(ctx, "Fields cannot be left blank",Toast.LENGTH_SHORT).show();
    }

    public static void enableFields(boolean enable, EditText... editTexts) {

        for (EditText editText:editTexts) {
            editText.setEnabled(enable);
        }

    }

    public static boolean checkIfEmpty(Activity activity, EditText... editTexts) {

        for (EditText editText:editTexts) {
            if (editText.getText().toString().trim().isEmpty()) {
                showEmptyToast(activity);
                return true;
            }
        }
        return false;

    }

    public static float getScreenDensity(Context activity) {
        float d = activity.getResources().getDisplayMetrics().density;
        return  d;
    }

    public static void showDialogWithMessage(Activity activity, String message) {

        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();

    }

    public static char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    public static String randomString(int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(CHARSET_AZ_09.length);
            result[i] = CHARSET_AZ_09[randomCharIndex];
        }
        return new String(result);
    }
}
