package com.ahmed.garbagecan.util.uiutil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ahmed.garbagecan.R;
import com.ahmed.garbagecan.listener.SaleItemRemovedListener;
import com.ahmed.garbagecan.model.entity.SaleDetail;

import java.util.ArrayList;

public class SaleDetailsAdapter extends BaseAdapter {

    private final SaleItemRemovedListener listener;
    Activity activity;
    ArrayList<SaleDetail> saleDetails;

    public SaleDetailsAdapter(Activity activity, ArrayList<SaleDetail> saleDetails, SaleItemRemovedListener listener) {
        this.activity = activity;
        this.saleDetails = saleDetails;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return saleDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.list_item_new, null);
        }

        final SaleDetail saleDetail = saleDetails.get(position);

        TextView txt_itemAndQuantity = (TextView) convertView.findViewById(R.id.txt_itemAndQuantity);
        TextView txt_size = (TextView) convertView.findViewById(R.id.txt_size);
        ImageButton btn_remove = (ImageButton) convertView.findViewById(R.id.btn_remove);

        txt_itemAndQuantity.setText(saleDetail.getItemName() + "  (" + saleDetail.getQuantity() + ")");
        txt_size.setText(saleDetail.getSize());

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemRemoved(saleDetail, position);
            }
        });

        return convertView;
    }
}
