package com.ahmed.garbagecan.listener;

import com.ahmed.garbagecan.model.entity.SaleDetail;

/**
 * Created by Ahmed on 6/10/2017.
 */

public interface SaleItemRemovedListener {

    public void onItemRemoved(SaleDetail saleDetail, int position);

}
