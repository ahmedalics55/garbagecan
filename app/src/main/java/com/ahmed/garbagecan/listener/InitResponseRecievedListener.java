package com.ahmed.garbagecan.listener;

/**
 * Created by Ahmed on 6/18/2017.
 */

public interface InitResponseRecievedListener {

    public void onInitResponseRecieved();

}
