package com.ahmed.garbagecan.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ahmed.garbagecan.R;
import com.ahmed.garbagecan.fragments.SaleFragment;
import com.ahmed.garbagecan.util.UtilMethods;
import com.ahmed.garbagecan.util.camera.CameraPreview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraActivityBelow19 extends Activity {
    private static String path1;

    public static String file;
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    public ImageButton capture, switchCamera;
    private Context myContext;
    private LinearLayout cameraPreview;
    private boolean cameraFront = false;
    private boolean cameraFrontAvailable = false;
    //    ImageView image;
    private boolean safeToTakePicture = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_photo);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;

        cameraFront = false;

        initialize();
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFrontAvailable = false;
            }
        }
        return cameraId;
    }

    public void onResume() {
        super.onResume();
        if (!hasCamera(myContext)) {
            Toast toast = Toast.makeText(myContext, "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
            finish();
        }

        Log.d("","my camera " + mCamera);

        if (mCamera == null) {
            if (findFrontFacingCamera() > -1) {
                releaseCamera();
                chooseCamera();
            } else {
                Toast.makeText(myContext, "Sorry, your phone does not support front camera!", Toast.LENGTH_LONG).show();
                releaseCamera();
                openBackCamera();
            }
            mCamera.startPreview();
        }
    };

    public void openBackCamera() {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                mCamera = Camera.open(cameraId);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
         else
               Toast.makeText(myContext, "Sorry, Could not load camera!", Toast.LENGTH_LONG).show();
    }

    public void chooseCamera() {
        int cameraId = -1;
        if (cameraFront)
            cameraId = findFrontFacingCamera();

         else
            cameraId = findBackFacingCamera();

        if (cameraId >= 0) {
            mCamera = Camera.open(cameraId);
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseCamera();
    }

    private boolean hasCamera(Context context) {
        //check if the device has camera
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                //make a new picture file
                com.ahmed.garbagecan.util.Log.i("qq -- cameraActivityBelow19");

                File pictureFile = getOutputMediaFile();

                if (pictureFile == null) {
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    Bitmap bitmap ;
                    if(cameraFront){
//                        bitmap = UtilMethods.rotateBitmap(UtilMethods.byteArrayToBitmap(data));
                        bitmap = UtilMethods.rotateBitmap(UtilMethods.byteArrayToBitmap(data));
                        bitmap = (Bitmap.createScaledBitmap(bitmap, 250, 200, false));
                    }
                    else{
                        bitmap = UtilMethods.rotateOnceBitmap(UtilMethods.byteArrayToBitmap(data));
                        int newWidth = getResources().getDisplayMetrics().widthPixels
                                        - ((int)(UtilMethods.getScreenDensity(myContext) * 80));
                        int newHeight = (bitmap.getHeight() / bitmap.getWidth()) * newWidth;;
                        bitmap = (Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false));
                    }
                    data = UtilMethods.bitmapToBytes(bitmap);
                    fos.write(data);
                    fos.close();
                    RotateImageIfNeeded(data, pictureFile);


                } catch (FileNotFoundException e) {
                } catch (IOException e) {
                } catch (Exception e) {
                }

                //refresh camera to continue preview
                mPreview.refreshCamera(mCamera);
            }
        };
        return picture;
    }

    private void RotateImageIfNeeded(byte[] bytes, File outputPic) {
        /////

        try {
            ExifInterface exifInterface = new ExifInterface(outputPic.getAbsolutePath());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap bitmap = UtilMethods.byteArrayToBitmap(bytes);


            if (orientation != 0) {
                switch(orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:

                    default:
                        break;
                }


            }



            Log.i("ahmed", "orientation: " + orientation);
            byte[] byteArray = UtilMethods.bitmapToBytes(bitmap);
            SaleFragment.imageBytes.add(byteArray);
            getIntent().setData(Uri.fromFile(outputPic));
            CameraActivityBelow19.this.setResult(RESULT_OK, CameraActivityBelow19.this.getIntent());
            CameraActivityBelow19.this.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
/////
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Log.i("ahmed", "orientation not ZERO -> bitmap rotated to: " + angle + " degrees");
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }



    View.OnClickListener captrureListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                mCamera.takePicture(null, null, mPicture);
            }
            catch (Exception e){}

        }
    };
    private File getOutputMediaFile() {
        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                UtilMethods.randomString(20));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        CameraActivityBelow19.file = mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg";

        return mediaFile;
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void initialize() {
        cameraPreview = (LinearLayout) findViewById(R.id.camera_preview);
        mPreview = new CameraPreview(myContext, mCamera, this);
        cameraPreview.addView(mPreview);

        capture = (ImageButton) findViewById(R.id.button_capture);
        capture.setOnClickListener(captrureListener);

        switchCamera = (ImageButton) findViewById(R.id.button_ChangeCamera);
        switchCamera.setOnClickListener(switchCameraListener);
    }

    View.OnClickListener switchCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //get the number of cameras
            int camerasNumber = Camera.getNumberOfCameras();
            if (camerasNumber > 1) {
                //release the old camera instance
                //switch camera, from the front and the back and vice versa

                releaseCamera();
                chooseCamera();
            } else {
                Toast toast = Toast.makeText(myContext, "Sorry, your phone has only one camera!", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CameraActivityBelow19.this.setResult(RESULT_CANCELED);
        CameraActivityBelow19.this.finish();
    }

}
