package com.ahmed.garbagecan.model.request;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class InitRequest {

    private String DeviceID;

    public InitRequest(String deviceID) {
        DeviceID = deviceID;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }
}
