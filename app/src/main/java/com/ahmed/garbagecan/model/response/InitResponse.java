package com.ahmed.garbagecan.model.response;

import com.ahmed.garbagecan.model.entity.Customer;
import com.ahmed.garbagecan.model.entity.Info;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class InitResponse {

    private Customer customer;
    private Info info;

    public InitResponse(Customer customer, Info info) {
        this.customer = customer;
        this.info = info;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }
}
