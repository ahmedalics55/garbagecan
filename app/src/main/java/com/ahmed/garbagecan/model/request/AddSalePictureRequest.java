package com.ahmed.garbagecan.model.request;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class AddSalePictureRequest {

    private int SaleId;

    public AddSalePictureRequest(int saleId) {
        SaleId = saleId;
    }

    public int getSaleId() {
        return SaleId;
    }

    public void setSaleId(int saleId) {
        SaleId = saleId;
    }
}
