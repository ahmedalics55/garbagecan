package com.ahmed.garbagecan.model.entity;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class SalePictures {

    private int id;
    private String FilePath;
    private int SaleID;

    public SalePictures(int id, String filePath, int saleID) {
        this.id = id;
        FilePath = filePath;
        SaleID = saleID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public int getSaleID() {
        return SaleID;
    }

    public void setSaleID(int saleID) {
        SaleID = saleID;
    }
}
