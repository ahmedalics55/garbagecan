package com.ahmed.garbagecan.model.request;

import com.ahmed.garbagecan.model.entity.SaleDetail;

import java.util.List;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class AddSaleRequest {

    private String DeviceId;
    private List<SaleDetail> Items;

    public AddSaleRequest(String deviceId, List<SaleDetail> items) {
        DeviceId = deviceId;
        Items = items;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public List<SaleDetail> getItems() {
        return Items;
    }

    public void setItems(List<SaleDetail> items) {
        Items = items;
    }
}
