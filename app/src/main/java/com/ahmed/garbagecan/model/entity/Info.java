package com.ahmed.garbagecan.model.entity;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class Info {

    private String Mobile;
    private String Email;
    private String AboutUs;

    public Info(String mobile, String email, String aboutUs) {
        Mobile = mobile;
        Email = email;
        AboutUs = aboutUs;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAboutUs() {
        return AboutUs;
    }

    public void setAboutUs(String aboutUs) {
        AboutUs = aboutUs;
    }
}
