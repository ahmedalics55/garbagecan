package com.ahmed.garbagecan.model.entity;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class Customer {

    private int id;
    private String Name;
    private String Mobile;
    private String Email;
    private String DeviceID;

    public Customer(int id, String name, String mobile, String email, String deviceID) {
        this.id = id;
        Name = name;
        Mobile = mobile;
        Email = email;
        DeviceID = deviceID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }
}
