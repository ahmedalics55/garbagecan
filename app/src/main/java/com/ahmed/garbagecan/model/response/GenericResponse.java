package com.ahmed.garbagecan.model.response;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class GenericResponse<T> {

    private String ResponseCode;
    private String ResponseDesc;
    private T Data;

    public GenericResponse(String responseCode, String responseDesc, T data) {
        ResponseCode = responseCode;
        ResponseDesc = responseDesc;
        Data = data;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseDesc() {
        return ResponseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        ResponseDesc = responseDesc;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }
}
