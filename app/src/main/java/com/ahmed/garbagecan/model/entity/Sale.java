package com.ahmed.garbagecan.model.entity;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class Sale {

    private int id;
    private int CustomerID;
    private String date;

    public Sale(int id, int customerID, String date) {
        this.id = id;
        CustomerID = customerID;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int customerID) {
        CustomerID = customerID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
