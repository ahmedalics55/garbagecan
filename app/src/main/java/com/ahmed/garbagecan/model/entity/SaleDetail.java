package com.ahmed.garbagecan.model.entity;

import android.widget.EditText;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class SaleDetail {

    private int id;
    private String ItemName;
    private String Quantity;
    private String Size;
    private int SaleID;

    public SaleDetail(int id, String itemName, String quantity, String size, int saleID) {
        this.id = id;
        ItemName = itemName;
        Quantity = quantity;
        Size = size;
        SaleID = saleID;
    }

    public SaleDetail(EditText itemName, EditText quantity, EditText size) {
        ItemName = itemName.getText().toString().trim();
        Quantity = quantity.getText().toString().trim();
        Size = size.getText().toString().trim();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getSaleID() {
        return SaleID;
    }

    public void setSaleID(int saleID) {
        SaleID = saleID;
    }
}
