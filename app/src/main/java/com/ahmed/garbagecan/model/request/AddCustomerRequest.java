package com.ahmed.garbagecan.model.request;

import android.widget.EditText;

/**
 * Created by Ahmed on 6/1/2017.
 */

public class AddCustomerRequest {

    private String Name;
    private String Mobile;
    private String Email;
    private String DeviceId;

    public AddCustomerRequest(EditText name, EditText mobile, EditText email, String deviceId) {
        Name = name.getText().toString();
        Mobile = mobile.getText().toString();
        Email = email.getText().toString();
        DeviceId = deviceId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }
}
