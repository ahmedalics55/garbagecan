package com.ahmed.garbagecan.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.ahmed.garbagecan.BaseFragment;
import com.ahmed.garbagecan.R;
import com.ahmed.garbagecan.util.Constants;
import com.ahmed.garbagecan.util.Log;

/**
 * Created by Ahmed on 4/29/2017.
 */

public class AboutUsFragment extends BaseFragment {

    private TextView txt_aboutUs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i("onViewCreated: view: " + view + ", savedInstanceState: " + savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
        txt_aboutUs = (TextView) view.findViewById(R.id.txt_aboutUs);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void updateInfo() {
        txt_aboutUs.setText(Constants.INFO == null? "OOPs" : Constants.INFO.getAboutUs());
    }
}
