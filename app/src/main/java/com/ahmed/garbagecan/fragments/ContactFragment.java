package com.ahmed.garbagecan.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ahmed.garbagecan.BaseFragment;
import com.ahmed.garbagecan.R;
import com.ahmed.garbagecan.util.Constants;
import com.ahmed.garbagecan.util.Log;

/**
 * Created by Ahmed on 4/29/2017.
 */

public class ContactFragment extends BaseFragment implements View.OnClickListener {

    private Button btn_mobile;
    private Button btn_email;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i("onViewCreated: view: " + view + ", savedInstanceState: " + savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

        btn_mobile = (Button) view.findViewById(R.id.btn_mobile);
        btn_email = (Button) view.findViewById(R.id.btn_email);



        btn_mobile.setOnClickListener(this);
        btn_email.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_mobile:

                break;

            case R.id.btn_email:

                break;


        }

    }

    public void updateInfo() {
        if (Constants.INFO == null || Constants.INFO.getMobile() == null ||
                Constants.INFO.getMobile().trim().isEmpty())
            btn_mobile.setVisibility(View.GONE);
        else
            btn_mobile.setText("Call Us: " + Constants.INFO.getMobile());

        if (Constants.INFO == null || Constants.INFO.getEmail() == null ||
                Constants.INFO.getEmail().trim().isEmpty())
            btn_email.setVisibility(View.GONE);
    }
}
