package com.ahmed.garbagecan.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.ahmed.garbagecan.BaseFragment;
import com.ahmed.garbagecan.MainActivity;
import com.ahmed.garbagecan.R;
import com.ahmed.garbagecan.activity.CameraActivity;
import com.ahmed.garbagecan.activity.CameraActivityBelow19;
import com.ahmed.garbagecan.listener.InitResponseRecievedListener;
import com.ahmed.garbagecan.listener.SaleItemRemovedListener;
import com.ahmed.garbagecan.model.entity.Customer;
import com.ahmed.garbagecan.model.entity.Sale;
import com.ahmed.garbagecan.model.entity.SaleDetail;
import com.ahmed.garbagecan.model.entity.SalePictures;
import com.ahmed.garbagecan.model.request.AddCustomerRequest;
import com.ahmed.garbagecan.model.request.AddSaleRequest;
import com.ahmed.garbagecan.model.request.InitRequest;
import com.ahmed.garbagecan.model.response.GenericResponse;
import com.ahmed.garbagecan.model.response.InitResponse;
import com.ahmed.garbagecan.util.Constants;
import com.ahmed.garbagecan.util.GarbageCanAPI;
import com.ahmed.garbagecan.util.GarbageCanService;
import com.ahmed.garbagecan.util.Log;
import com.ahmed.garbagecan.util.UtilMethods;
import com.ahmed.garbagecan.util.uiutil.SaleDetailsAdapter;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION.SDK_INT;
import static com.ahmed.garbagecan.util.Constants.INFO;
import static com.ahmed.garbagecan.util.Constants.LOGGED_IN_CUSTOMER;

/**
 * Created by Ahmed on 4/29/2017.
 */

public class SaleFragment extends BaseFragment implements View.OnClickListener, SaleItemRemovedListener {

    private InitResponseRecievedListener initResponseRecievedListener;

    public SaleFragment() {
    }

    public SaleFragment(InitResponseRecievedListener listener) {
        this.initResponseRecievedListener = listener;
    }

    private String TAG = "ahmed1";
    private EditText editText_name, editText_mobile, editText_email;
    private EditText editText_item, editText_size, editText_quantity;

    private ListView listView_items;
    private ImageView imageView_selected;
    private LinearLayout linear_thumbnails;

    private Button btn_takePicture, btn_addItem, btn_submit;

    public static ArrayList<byte[]> imageBytes = new ArrayList<>();
    public static ArrayList<File> files = new ArrayList<>();

    private ArrayList<Bitmap> thumbnails = new ArrayList<>();
    private ScrollView scrollView;
    private int selectedTag = -1;

    ProgressDialog progressDialog;
    private GarbageCanAPI service;

    public static String file;

    public static int count = 0;
    private ArrayList<SaleDetail> saleDetails = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if (!Constants.IS_APP_INITIALIZED)
            initiateRequestInit();
    }

    private void initiateRequestInit() {
        progressDialog.show();
        service = GarbageCanService.getService();
        Constants.IS_APP_INITIALIZED = true;
        Call<GenericResponse<InitResponse>> callback = service.init(new InitRequest(Constants.DEVICE_ID));
        callback.enqueue(new Callback<GenericResponse<InitResponse>>() {
            @Override
            public void onResponse(Call<GenericResponse<InitResponse>> call, Response<GenericResponse<InitResponse>> response) {

                progressDialog.dismiss();
                if (!response.isSuccessful()) {
                    Log.i("INIT response not successful");
                    UtilMethods.showFailureToast(getActivity());
                    return;
                }

                InitResponse initResponse = response.body().getData();
                LOGGED_IN_CUSTOMER = initResponse.getCustomer();
                INFO = initResponse.getInfo();
                if (LOGGED_IN_CUSTOMER != null) {
                    editText_name.setText(LOGGED_IN_CUSTOMER.getName());
                    editText_email.setText(LOGGED_IN_CUSTOMER.getEmail());
                    editText_mobile.setText(LOGGED_IN_CUSTOMER.getMobile());
                    UtilMethods.enableFields(false, editText_name, editText_mobile, editText_email);
                }
                else {
                    editText_name.setText("");
                    editText_email.setText("");
                    editText_mobile.setText("");
                    UtilMethods.enableFields(true, editText_name, editText_mobile, editText_email);
                }
                initResponseRecievedListener.onInitResponseRecieved();

            }

            @Override
            public void onFailure(Call<GenericResponse<InitResponse>> call, Throwable t) {
                progressDialog.dismiss();

                UtilMethods.showFailureToast(getActivity());
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("OnCreateView: savedInstanceState: " + savedInstanceState );
        return inflater.inflate(R.layout.fragment_sale, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i("onViewCreated: view: " + view + ", savedInstanceState: " + savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

        editText_name = (EditText) view.findViewById(R.id.editText_name);
        editText_mobile = (EditText) view.findViewById(R.id.editText_mobile);
        editText_email = (EditText) view.findViewById(R.id.editText_email);
        editText_item = (EditText) view.findViewById(R.id.editText_item);
        editText_size = (EditText) view.findViewById(R.id.editText_size);
        editText_quantity = (EditText) view.findViewById(R.id.editText_quantity);
        linear_thumbnails = (LinearLayout) view.findViewById(R.id.linear_thumbnails);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        imageView_selected = (ImageView) view.findViewById(R.id.imageView_selected);

        listView_items = (ListView) view.findViewById(R.id.listView_items);
        btn_addItem = (Button)  view.findViewById(R.id.btn_addItem);
        btn_takePicture = (Button)  view.findViewById(R.id.btn_takePicture);
        btn_submit = (Button)  view.findViewById(R.id.btn_submit);

        btn_addItem.setOnClickListener(this);
        btn_takePicture.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {

                files.add(new File(data.getData().getPath()));
                byte[] temp = imageBytes.get(imageBytes.size() - 1);


                Bitmap bitmap = UtilMethods.byteArrayToBitmap(temp);

                Bitmap smallerbitmap = UtilMethods.getSmallBitmap(bitmap);
                thumbnails.add(smallerbitmap);

                Log.i("thumbnails: " + thumbnails.size());
                Log.i("imageBytes: " + imageBytes.size());

                addThumbnailSetImage(smallerbitmap, bitmap);


            }
        }
    }

    private void addThumbnailSetImage(Bitmap smallerbitmap, final Bitmap bitmap) {

        Log.i("bitmap: " + bitmap);
        imageView_selected.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
        thumbnails.add(smallerbitmap);

        addThumbnail(smallerbitmap);

    }

    private void addThumbnail(Bitmap smallerbitmap) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        final View view = inflater.inflate(R.layout.thumbnail, null);

        ImageView imageView_thumbnail = (ImageView) view.findViewById(R.id.imageView_thumbnail);
        ImageButton btn_removeThumbnail = (ImageButton) view.findViewById(R.id.btn_removeThumbnail);

        imageView_thumbnail.setImageBitmap(smallerbitmap);
        btn_removeThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                linear_thumbnails.removeView(view);
                view.setVisibility(View.GONE);
                if(Integer.parseInt(String.valueOf(view.getTag())) == selectedTag) {
                    if (linear_thumbnails.getChildCount() == 0) {
                        imageView_selected.setImageBitmap(null);
                        selectedTag = -1;
                    }
                    else {
                        View temp = linear_thumbnails.getChildAt(0);
                        changeImageViewSelected(temp);
                    }

                }

            }
        });
        view.setTag(imageBytes.size()-1);
        selectedTag = imageBytes.size();

        imageView_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeImageViewSelected(view);
            }
        });

        linear_thumbnails.addView(view);

    }

    private void changeImageViewSelected(View view) {
        byte[] temp = imageBytes.get(Integer.parseInt(String.valueOf(view.getTag())) - 1);
        Bitmap bitmap = UtilMethods.byteArrayToBitmap(temp);
        imageView_selected.setImageBitmap(bitmap);
        selectedTag = Integer.parseInt(String.valueOf(view.getTag()));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_addItem:

                if (UtilMethods.checkIfEmpty(getActivity(), editText_item, editText_quantity, editText_size))
                    return;

                SaleDetail saleDetail = new SaleDetail(editText_item, editText_quantity, editText_size);
                refreshList(saleDetail);
                break;



            case R.id.btn_takePicture:
                if(SDK_INT >= 23) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                        return;
                    }
                }

                if(SDK_INT >= 21) {
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivityForResult(intent, 200);
                }
                else
                    openCameraForBelow21();

                break;

            case R.id.btn_submit:

                if (UtilMethods.checkIfEmpty(getActivity(), editText_name, editText_mobile, editText_email))
                    return;

                if (LOGGED_IN_CUSTOMER == null)
                    initiateRequestForAddCustomer();
                else
                    initiateRequestForSale();


                break;
        }

    }

    private void initiateRequestForAddCustomer() {

        progressDialog.show();

        Call<GenericResponse<Customer>> callback = service.addCustomer(new AddCustomerRequest(editText_name, editText_mobile, editText_email, Constants.DEVICE_ID));
        callback.enqueue(new Callback<GenericResponse<Customer>>() {
            @Override
            public void onResponse(Call<GenericResponse<Customer>> call, Response<GenericResponse<Customer>> response) {
                Log.i("addCustomer onResponse");
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    GenericResponse<Customer> addCustomerResponse = response.body();

                    if (addCustomerResponse.getResponseCode().equals("00")) {
                        LOGGED_IN_CUSTOMER = addCustomerResponse.getData();
                        initiateRequestForSale();

                    }
                    else {
                        UtilMethods.showDialogWithMessage(getActivity(), addCustomerResponse.getResponseDesc());
                    }


                }
                else {
                    UtilMethods.showFailureToast(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<Customer>> call, Throwable t) {
                Log.i("addCustomer onFailure");
                progressDialog.dismiss();
                UtilMethods.showFailureToast(getActivity());
            }
        });
    }

    private void initiateRequestForSale() {

        Log.i("Calling sale");
        progressDialog.show();
        Call<GenericResponse<Sale>> callback = service.addSale(new AddSaleRequest(Constants.DEVICE_ID, saleDetails));
        callback.enqueue(new Callback<GenericResponse<Sale>>() {
            @Override
            public void onResponse(Call<GenericResponse<Sale>> call, Response<GenericResponse<Sale>> response) {
                Log.i("sale onResponse");
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    GenericResponse<Sale> saleResponse = response.body();

                    if (saleResponse.getResponseCode().equals("00")) {

                        initiateRequestForUpload(saleResponse.getData());

                    }
                    else {
                        UtilMethods.showDialogWithMessage(getActivity(), saleResponse.getResponseDesc());
                    }


                }
                else {
                    UtilMethods.showFailureToast(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<Sale>> call, Throwable t) {
                Log.i("sale onFailure");
                UtilMethods.showFailureToast(getActivity());

            }
        });

    }

    private void initiateRequestForUpload(Sale sale) {

//        File pictureFile = getOutputMediaFile(imageBytes.get(0));
        progressDialog.show();
        int picturesCount = 0;
        final int[] uploadedPictureCount = {0};

        for (int i = 0; i <= linear_thumbnails.getChildCount()-1; i++) {

            if (linear_thumbnails.getChildAt(i).getVisibility() == View.GONE)
                continue;

            picturesCount++;

            final File pictureFile = files.get(i);
            Log.i("pictureFile: " + pictureFile.getName());

            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", pictureFile.getName(), RequestBody.create(MediaType.parse("image/*"), pictureFile));
            Call<GenericResponse<SalePictures>> callback = service.upload(sale.getId(), filePart);
            final int finalPicturesCount = picturesCount;
            callback.enqueue(new Callback<GenericResponse<SalePictures>>() {
                @Override
                public void onResponse(Call<GenericResponse<SalePictures>> call, Response<GenericResponse<SalePictures>> response) {
                    Log.i("picture upload onResponse: " + (new Gson().toJson(response.body())));
                    uploadedPictureCount[0]++;
                    pictureFile.delete();

                    if (uploadedPictureCount[0] >= finalPicturesCount) {
                        progressDialog.dismiss();
                        resetScreen();
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse<SalePictures>> call, Throwable t) {
                    Log.i("picture upload onFailure: " + t.getMessage());
                    uploadedPictureCount[0]++;
                    pictureFile.delete();

                    if (uploadedPictureCount[0] >= finalPicturesCount) {
                        progressDialog.dismiss();
                        resetScreen();
                    }

                }
            });
        }



    }

    private void resetScreen() {
        Log.i("Images uploaded. Resetting screen now");
        imageBytes = new ArrayList<>();
        files = new ArrayList<>();
        saleDetails = new ArrayList<>();
        thumbnails = new ArrayList<>();
        selectedTag = -1;
        linear_thumbnails.removeAllViews();
        imageView_selected.setImageDrawable(null);

        refreshList(null);

    }

    private void refreshList(SaleDetail saleDetail) {

        if (saleDetail != null)
            saleDetails.add(saleDetail);
        listView_items.setAdapter(new SaleDetailsAdapter(getActivity(), saleDetails, this));
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("onResume() child: " + linear_thumbnails.getChildCount());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("onPause() child: " + linear_thumbnails.getChildCount());
    }

    private void openCameraForBelow21() {
        count++;
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        file = dir+count+".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
        }

        Uri outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(getActivity(), CameraActivityBelow19.class);

        startActivityForResult(cameraIntent, 200);

    }

    @Override
    public void onItemRemoved(SaleDetail saleDetail, int position) {

        saleDetails.remove(position);
        refreshList(null);

    }
}
